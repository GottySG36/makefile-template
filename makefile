# Directory where Makefile is located
DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))/go-tools

# Directory where go compiler will be downloaded
GODOWNLOAD=$(DIR)/.golang

# Output directory in which the compiled binaries will be located
BINDIR=$(DIR)/bin
SRCDIR=$(DIR)/src
PKGDIR=$(DIR)/pkg

GOCMD=go
BUILD=$(GOCMD) build
GETSRC=$(GOCMD) get

##############################
## Modifications start here ##


# Go parameters
# May be changed to match newer version
# or to match different OS/ARCH
OS=linux
ARCH=amd64
VERSION=1.15

# REPO list to get go projects/tools, space seperated
# Go requires a particular format to be able to download project repos.
# - The VCS address ie : github.com, bitbucket.org, ...
# - The repo owner
# - The repo name, without the trailing '.git'
REPO='<VCS NAME>/<REPO OWNER>/<PACKAGE>'

# If there are any particular flags to send to the compiler... 
FLAGS=""

## Nothing to do below here ##
##############################


# Additional includes and environment variables definition
INC=$(DIR)/.includes.mk
VAR=$(DIR)/.variables.sh

-include $(INC)

all: init get-go get-source build-all

get-source:
	. $(VAR); mkdir -p $(SRCDIR); chmod -R 770 $(SRCDIR); cd $(SRCDIR); for src in $(REPO); do echo "Downloading $$src ..."; $(GETSRC) $$src; done

# Any and all buil rules added above, space seperated
build-all: 
	. $(VAR); for src in $(REPO); do cd $(DIR)/src/$$src ; echo "Building $$src ..."; $(BUILD) $(FLAGS) -o $(BINDIR) -v; cd $(DIR); done;
	chmod -R 770 $(SRCDIR) $(PKGDIR);


init:
	if [ ! -d $(DIR) ]; then mkdir -p $(DIR); fi;
	if [ ! -e $(INC) ]; then touch $(INC); fi;
	if [ ! -e $(VAR) ]; then touch $(VAR); fi;
	if [ ! -e $(SRCDIR) ]; then mkdir $(SRCDIR); fi;
	if [ ! -e $(BINDIR) ]; then mkdir $(BINDIR); fi;
	if [ ! -e $(PKGDIR) ]; then mkdir $(PKGDIR); fi;
	if [ ! -e $(GODOWNLOAD) ]; then mkdir $(GODOWNLOAD); fi;

# Other utilities
## Download go
get-go:
	if [ ! -e $(GODOWNLOAD) ]; then mkdir $(GODOWNLOAD); fi;
	wget -O- https://golang.org/dl/go$(VERSION).$(OS)-$(ARCH).tar.gz | tar -C $(GODOWNLOAD) -xzf -;
	echo GOCMD=$(GODOWNLOAD)/go/bin/go > $(INC);
	echo '#!/bin/bash' > $(VAR);
	echo 'export PATH=$(GODOWNLOAD)/go/bin:$$PATH' >> $(VAR);
	echo 'export GOPATH=$(DIR)' >> $(VAR);

reset: clean-all init

# Remove downloaded go compiler
clean-all: clean
	if [ -e $(BINDIR) ]; then rm -rf $(BINDIR); fi;
	mkdir -p $(DIR)
	touch $(INC) $(VAR)
	mkdir $(BINDIR)
	mkdir $(PKGDIR)
	mkdir $(GODOWNLOAD)

# Remove go binaries and resets includes and env variable definition files
clean:
	if [ -e $(INC) ]; then rm -f $(INC); fi;
	if [ -e $(VAR) ]; then rm -f $(VAR); fi;
	if [ -e $(SRCDIR) ]; then rm -rf $(SRCDIR); fi;
	if [ -e $(PKGDIR) ]; then rm -rf $(PKGDIR); fi;
	if [ -e $(GODOWNLOAD) ]; then rm -rf $(GODOWNLOAD); fi;
	if [ -d $(DIR) ]; then rm -rf $(DIR); fi;
